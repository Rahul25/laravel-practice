@extends('layouts.main-layout')

@section('content')
    @guest
        <h1>This Is a Home page</h1>
    @else
        <h1>Welcome {{ Auth::user()->name }}</h1>
    @endguest
@endsection
