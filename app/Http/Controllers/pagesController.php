<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class pagesController extends Controller
{
    public $layout = 'layouts.main-layout';

    public function about() {
        if ( View::exists('about.about') ) {
            $name = 'Rahim';
            $age = 23;
            $pagetitle = 'About Page';
            $pageLayout = $this->layout;
            $view = View::make('about.about',compact(['name', 'age', 'pagetitle', 'pageLayout']));
        }
        return $view;
    }

    public function service() {
        if( View::exists('service.service') ) {
            $pagetitle = 'Service Page';
            $pageLayout = $this->layout;
            $view = View::make( 'service.service',compact(['pageLayout', 'pagetitle']) );
        }
        return $view;
    }
}
